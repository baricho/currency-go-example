package main

import (
	"net/http"
	"log"
	"io"
	"fmt"
	"net/http/httputil"
	"regexp"
	"strings"
)

func XeCurrencyServer(w http.ResponseWriter, req *http.Request) {

	cur := strings.Split(req.URL.Path,"/")
	fmt.Fprintf(w, "<html><body><br><br><br>")
	if cur[1] == "USD" || cur[1] == "GBP" || cur[1] == "EUR" {

		currency_requested := ("http://www.xe.com/currencyconverter/convert/?Amount=1&From="+cur[1]+"&To=ETB")
        	response, err := http.Get(currency_requested)
	
		var currencyRegex = regexp.MustCompile(`\&\w+;`+cur[1]+`\&\w+;=\&\w+;(\d+\.\d+)\&\w+;ETB`)
        	if err != nil {
                	log.Fatal(err)
       		} else {
                	defer response.Body.Close()
			dump, err := httputil.DumpResponse(response, true)
			bodyString := string(dump) 
			conversion := currencyRegex.FindStringSubmatch(bodyString)
			for k, v := range conversion {
				if k > 0 {
					fmt.Fprintf(w, "1.00 %s == %s ETB", cur[1], v)
				}
			}
                	if err != nil {
				io.WriteString(w,"Error communicating with xe.com")
                        	log.Fatal(err)
                	}
        	}
	} else {
		fmt.Fprintf(w, "Currency requested not found: %s", cur[1])
	}
	fmt.Fprintf(w, "</body></html>")
}

func main() {
	http.HandleFunc("/", XeCurrencyServer)
	
	log.Fatal(http.ListenAndServe(":12345", nil))
}
